﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
//using System.Json

namespace ShipStationApi
{
    class Program
    {
        static void Main(string[] args)
        {
            String response;
            response = ShipStationShipmentCall();
            ShipStationRefreshCall();
            //" + storeId + "

        }
        public static async Task<String> ShipStationCallAsync()
        {
            var baseAddress = new Uri("https://ssapi.shipstation.com/");

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {
                
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("authorization", "Basic MTI2NDdmMzg5YWI3NDcyZDg1ZTEwMTNlYmQ4ZjkyNjU6ZTNhMzA2MmQ1ZDFhNDI1MTgxMzY1ZTc2YWM1NjlmZWM=");

                using (var response = await httpClient.GetAsync("shipments?includeShipmentItems=true"))
                {

                    string responseData = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseData);
                    return responseData;
                }
            }
            
        }
        public class shipTo 
        {
           public String name { get; set; }
           public String company { get; set; }
           public String street1 { get; set; }
           public String street2 { get; set; }
           public String street3 { get; set; }
            public String city { get; set; }
            public String state { get; set; }
            public String postalCode  { get; set; }
            public String country { get; set; }
            public String phone { get; set; }
            public String residential { get; set; }
            public String addressVerified  { get; set; }
    }   
        public class weight
        {
            public double value { get; set; }
            public String units { get; set; }
            public double WeightUnits { get; set; }
        }
        public class dimensions
        {
            public String units { get; set; }
            public double length { get; set; }
            public double width { get; set; }
            public double height { get; set; }
        
        }
        public class insuranceOptions
        {
            public String provider { get; set; }
            public bool insureShipment { get; set; }
            public double insuredValue { get; set; }
         
        }
        public class shipmentItems
        {
            public int orderItemId { get; set; }
            public String lineItemKey { get; set; }
            public String sku { get; set; }
            public String name { get; set; }
            public String imageUrl { get; set; }
            public weight weight { get; set; }
            public int quantity { get; set; }
            public double unitPrice { get; set; }
            public double taxAmount { get; set; }
            public String warehouseLocation { get; set; }
            public String options { get; set; }
            public int productId { get; set; }
            public String fulfillmentSku { get; set; }
            public bool adjustment { get; set; }
            public String upc { get; set; }
            public String createDate { get; set; }
            public String modifyDate { get; set; }


        }
        public class advancedOptions
        {
            public String billToParty { get; set; }
            public String billToAccount { get; set; }
            public String billToPostalCode { get; set; }
            public String billToCountryCode { get; set; }
            public int storeId { get; set; }
        }

        public class shipment
        {
            public int shipmentId { get; set; }
            public int orderId { get; set; }
            public String orderKey { get; set; }
            public String userId { get; set; }
            public String customerEmail { get; set; }
            public String orderNumber { get; set; }
            public DateTime createDate { get; set; }
            public DateTime shipDate { get; set; }
            public double shipmentCost { get; set; }
            public double insuranceCost { get; set; }
            public String trackingNumber { get; set; }
            public String isReturnLabel { get; set; }
            public String carrierCode { get; set; }
            public String serviceCode { get; set; }
            public String packageCode { get; set; }
            public int warehouseId { get; set; }
            public String voided { get; set; }
            public String voidDate { get; set; }
            public String marketplaceNotified { get; set; }
            public String notifyErrorMessage { get; set; }
            public shipTo shipTo { get; set; }
            public weight weight { get; set; }
            public dimensions dimensions { get; set; }
            public insuranceOptions insuranceOptions { get; set; }
            public advancedOptions advancedOptions { get; set; }
            public List<shipmentItems> shipmentItems { get; set; }
            public String labelData { get; set; }
            public String formData { get; set; }

        }
        public class shipmentAll
        {
            public List<shipment> shipments { get; set; }
        }
        public class ItemOption
        {
            public string name { get; set; }
            public string value { get; set; }
        }
        public class OrderItem
        {
            public int orderItem { get; set; }
            public string lineItemKey { get; set; }
            public string sku { get; set; }
            public string name { get; set; }
            public string imageUrl { get; set; }
            public weight weight { get; set; }
            public double quantity { get; set; }
            public double unitPrice { get; set; }
            public double taxAmount { get; set; }
            public double shippingAmount { get; set; }
            public string warehouse { get; set; }
            public List<ItemOption> ItemOtion { get; set; }
            public string productId { get; set; }
            public string fulfillmentSku { get; set; }
            public bool adjustment { get; set; }
            public string upc { get; set; }
            public string createDate { get; set; }
            public string  modifyDate { get; set; }

        }
        public static String ShipStationRefreshCall()
        {
            var baseAddress = new Uri("https://ssapi.shipstation.com/");

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("authorization", "Basic MTI2NDdmMzg5YWI3NDcyZDg1ZTEwMTNlYmQ4ZjkyNjU6ZTNhMzA2MmQ1ZDFhNDI1MTgxMzY1ZTc2YWM1NjlmZWM=");
                StringContent content = new StringContent("");
                    //new StringContent("{\"storeID\":328982 ,\"refreshDate\": \"08-16-2017\"}", System.Text.Encoding.Default, "application/json");
                var response = httpClient.PostAsync("stores/refreshstore?", content).Result;
                if (response.IsSuccessStatusCode)
                {
                  
                    //  var shipments = JsonConvert.DeserializeObject(json);
                    return "Sucess";
                }
                else
                {
                    return "Failed";
                }

                //using (var response = httpClient.GetAsync("shipments?includeShipmentItems=true"))
                //{

                //    string responseData = response.Content.ReadAsStringAsync();
                //    Console.WriteLine(responseData);
                //    return responseData;
                //}
            }
        }
        public static String ShipStationShipmentCall()
        {
            var baseAddress = new Uri("https://ssapi.shipstation.com/");

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("authorization", "Basic MTI2NDdmMzg5YWI3NDcyZDg1ZTEwMTNlYmQ4ZjkyNjU6ZTNhMzA2MmQ1ZDFhNDI1MTgxMzY1ZTc2YWM1NjlmZWM=");
                HttpResponseMessage response = httpClient.GetAsync("shipments?includeShipmentItems=true").Result;
                if(response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync();
                   string json =  data.Result; 
                    shipmentAll shipments = JsonConvert.DeserializeObject<shipmentAll>(json);
                    foreach (shipment s in shipments.shipments)
                    {
                        ShipStation_ShipmentInsert(s);
                    }
                    //  var shipments = JsonConvert.DeserializeObject(json);
                    return "Sucess";
                }
                else
                {
                    return "Failed";
                }

                //using (var response = httpClient.GetAsync("shipments?includeShipmentItems=true"))
                //{

                //    string responseData = response.Content.ReadAsStringAsync();
                //    Console.WriteLine(responseData);
                //    return responseData;
                //}
            }
        }
        public static void ShiStationCreateOrders()
        {
            
        }
        public static void ShipStation_ShipmentInsert(shipment s)
        {
            string csidbconnstring;
            SqlConnection csiconn;
            csidbconnstring = "Data Source=CSI-DB;Initial Catalog=KencoveCSI;Integrated Security=True";
            csiconn = new SqlConnection(csidbconnstring);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "EXEC ShipStation_InsertShipment " + s.shipmentId  + ", " + s.orderId + "" + ", '" + s.orderKey + "'" + ", '" + s.userId + "'" + ", '" 
                + s.customerEmail + "'" + ", '" + s.orderNumber + "'" + ", '" + s.createDate + "'" + ", '" + s.shipDate + "'" + ", " 
                + s.shipmentCost + "" + ", " + s.insuranceCost + "" + ", '" + s.trackingNumber + "'" + ", '" + s.isReturnLabel + "'" 
                + ", '" + "" + "'" + ", '" + s.carrierCode + "'" + ", '" + s.serviceCode + "'" + ", '" + s.packageCode + "'" 
                + ", '" + "" + "'" + ", " + s.warehouseId + "" + ", '" + s.voided + "'" + ", '" + s.voidDate + "'" + ", '" + s.marketplaceNotified 
                + "'" + ", '" + s.notifyErrorMessage + "'" + ", '" + s.labelData + "'" + ", '" + s.formData + "'" + ", " + s.weight.value + "" + ", '" 
                + s.weight.units + "'" + ", " + s.weight.WeightUnits + "" + ", '" + s.dimensions.units + "'" + ", " + s.dimensions.length + "" + ", " + s.dimensions.width + "" 
                + ", " + s.dimensions.height + "" + ", '" + s.insuranceOptions.provider + "'" + ", '" + s.insuranceOptions.insureShipment + "'" + ", " + s.insuranceOptions.insuredValue + "" + ", '" 
                + s.advancedOptions.billToParty + "'" + ", '" + s.advancedOptions.billToAccount + "'" + ", '" + s.advancedOptions.billToPostalCode + "'" + ", '" + s.advancedOptions.billToCountryCode + "'" 
                + ", '" + s.advancedOptions.storeId + "'";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = csiconn;
            try
            {
                csiconn.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }


            csiconn.Close();
            foreach(shipmentItems si in s.shipmentItems)
            {
                ShipStation_ShipItemInsert(si, s.shipmentId);
            }

        }
        public static void ShipStation_ShipItemInsert(shipmentItems si,int shipmentId)
        {
            string csidbconnstring;
            SqlConnection csiconn;
            csidbconnstring = "Data Source=CSI-DB;Initial Catalog=KencoveCSI;Integrated Security=True";
            csiconn = new SqlConnection(csidbconnstring);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "EXEC ShipStation_InsertShipItem " + "" + shipmentId + ""
            + "," + si.orderItemId + ""
            + ",'" + si.lineItemKey + "'"
            + ",'" + si.sku + "'"
            + ",'" + si.name + "'"
            + ",'" + stripNonValidXMLCharacters(si.imageUrl) + "'"
            + "," + si.quantity + ""
            + "," + si.unitPrice + ""
            + "," + si.taxAmount + ""
            + "," + 0 + ""
            + ",'" + si.warehouseLocation + "'"
            + ",'" + si.options + "'"
            + "," + si.productId + ""
            + ",'" + si.fulfillmentSku + "'"
            + ",'" + si.adjustment + "'"
            + ",'" + si.upc + "'"
            + ",'" + si.createDate + "'"
            + ",'" + si.modifyDate + "'"
            + "," + si.weight.value + ""
            + ",'" + si.weight.units + "'"
            + "," + si.weight.WeightUnits + "";
            cmd.Connection = csiconn;
            try
            {
                csiconn.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }


            csiconn.Close();

        }
        public static string stripNonValidXMLCharacters(string textIn)
        {
            StringBuilder textOut = new StringBuilder(); // Used to hold the output.
            char current; // Used to reference the current character.


            if (textIn == null || textIn == string.Empty) return string.Empty; // vacancy test.
            for (int i = 0; i < textIn.Length; i++)
            {
                current = textIn[i];

                // && current != 0x00BD && (current >= 0x2153 && current <= 0x215F)
                if (
                    ((current == 0x9 || current == 0xA || current == 0xD) ||
                    ((current >= 0x20) && (current <= 0xD7FF)) ||
                    ((current >= 0xE000) && (current <= 0xFFFD)) ||
                    ((current >= 0x10000) && (current <= 0x10FFFF)))
                    && current != 0x00BD && !(current >= 0x2153 && current <= 0x215F)
                    )
                {
                    textOut.Append(current);
                }
            }
            return textOut.ToString();
        }
    }
}
